<?php

namespace Drupal\ajax_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ajax_example\Form\AjaxExampleForm;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * The module controller class.
 */
class AjaxExampleController extends ControllerBase {

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a SamenwerkingController object.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The active menu trail service.
   */
  public function __construct(FormBuilderInterface $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * AJAX callback.
   */
  public function callback(Request $request) {
    $response = new AjaxResponse();

    // Pass parameters to the form.
    $parameters = [
      'some_parameter' => $request->query->get('some_parameter', ''),
    ];
    $form = $this->formBuilder->getForm(AjaxExampleForm::class, $parameters);

    drupal_set_message($form['link']['#title']);

    $form['messages'] = [
      '#type' => 'status_messages',
      '#weight' => -100,
    ];

    // Important: Pass the form as a renderable array, not rendered
    // HTML, otherwise AJAX functionality will be lost.
    $response->addCommand(new ReplaceCommand(
      $form['#ajax_selector'],
      $form
    ));

    return $response;
  }

}
