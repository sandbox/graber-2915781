<?php

namespace Drupal\ajax_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form definition for factors.
 */
class AjaxExampleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajax_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $parameters = []) {
    $container_id = 'ajax-example-output-wrapper';
    $form['#ajax_selector'] = '#' . $container_id;

    $form['#prefix'] = "<div id=\"$container_id\">";
    $form['#suffix'] = "</div>";

    $ajax = [
      'callback' => [$this, 'ajaxCallback'],
      'wrapper' => $container_id,
    ];

    $form['output'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $container_id,
      ],
    ];
    $form['link'] = [
      '#type' => 'link',
      '#title' => $this->t('AJAX link'),
      '#url' => Url::fromRoute('ajax_example.example_form_ajax', [], ['query' => ['some_parameter' => 'some_value']]),
      '#attributes' => ['class' => ['use-ajax']],
    ];

    $form['button'] = [
      '#type' => 'button',
      '#value' => $this->t('AJAX button'),
      '#ajax' => $ajax,
      '#weight' => -10,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('AJAX submit'),
      '#ajax' => $ajax,
    ];

    return $form;
  }

  /**
   * Ajax callback.
   */
  public function ajaxCallback(array $form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    drupal_set_message($trigger['#value']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
